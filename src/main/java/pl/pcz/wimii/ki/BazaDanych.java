package pl.pcz.wimii.ki;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class BazaDanych {
    private static BazaDanych instance = null;

    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:~/mddd";

    //  Database credentials
    private static final String USER = "mddd";
    private static final String PASS = "md";

    private Connection conn = null;

    private BazaDanych() {
        System.out.println("Utworzono obiekt baza danych!");
        registerDriver();
    }

    public static BazaDanych getInstance() {
        if (instance == null) {
            instance = new BazaDanych();
        }
        return instance;
    }

    private void registerDriver(){
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void openConnection()
    {
        if (conn == null) {
            try {
                conn = DriverManager.getConnection(DB_URL,USER,PASS);
            } catch (SQLException e) {
                System.out.println(e);
            } finally {
                System.out.println("Polaczenie z baza danych nawiazane");
            }
        }
    }

    public void closeConnection() {
        try {
            if(conn!=null) conn.close();
        } catch(SQLException se){
            se.printStackTrace();
        } finally {
            System.out.println("Zamknieto polaczenie z baza danych");
        }
    }
}
